<!DOCTYPE html>
<html>
	<?php require('header.php'); ?>
	<body style="padding-top: 10%;">
		<div align="center">
			<div id="create_id">
				<h2>Create a record</h2>
				<table border="1">
					<tr>
						<th>Name</th>
						<td><input type="text" id="c_name"></td>
					</tr>
					<tr>
						<th>Description</th>
						<td><input type="text" id="c_desc"></td>
					</tr>
					<tr>
						<th>Course</th>
						<td>
							<select id="c_course">
								<option value="0">-- SELECT COURSE --</option>
								<option value="IT">IT</option>
								<option value="CRIM">CRIM</option>
								<option value="EDUC">EDUC</option>
								<option value="ENG">ENG</option>
								<option value="HRM">HRM</option>	
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><button onclick="create_data()">Submit</button> | <a onclick="cancel()" href="index.php?page=main">Cancel</a> </td>
					</tr>
				</table>
			</div>
		</div>

	</body>
</html>