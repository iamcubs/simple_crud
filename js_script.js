
function cancel(){
	$('#c_name').val('');
	$('#c_desc').val('');
	$('#c_course').val(0);	
}

function create_data(){
	$name = $('#c_name').val();
	$desc = $('#c_desc').val();
	$course = $('#c_course').val();

	if($name == '' || $desc == '' || $course == 0){
		alert('Please fill up all fields');
	}else{
		$.ajax({
			url: 'routes.php?route=create',
			type: 'POST',
			data: {'name': $name, 'desc': $desc, 'course': $course},
			success:function(data){
				// alert(data) -->PLEASE use this if you want to know what is the response from the database data in case if there is an error
				if(data == 'success'){
					cancel();
					alert('Successfully created a record!');
				}else{
					alert('Sorry Error');
				}
			}
		});
	}
}

function update_list($id){
	$.ajax({
		url: 'routes.php?route=getdata',
		type: 'POST',
		data: {'id': $id},
		success:function(data){
			$('#update_div').show();
			$('#update_tbl').html(data);
		}
	});
}


function edit_user(){
	$edt_id = $('#edt_id').val();
	$edt_name = $('#edt_name').val();
	$edt_desc = $('#edt_desc').val();
	$edt_course = $('#edt_course').val();

	$.ajax({
		url: 'routes.php?route=update',
		type: 'POST',
		data: {'edt_name': $edt_name, 'edt_desc': $edt_desc, 'edt_course': $edt_course, 'edt_id': $edt_id},
		success:function(data){
			if(data == 'success'){
				$('#list_id').load('index.php?page=main #list_id');
				$('#update_div').hide();
				alert($edt_name+' has been successfully updated!');
			}else{
				alert('Error');
			}
		}
	});

}


function delete_list($id){
	if(confirm('Are you sure you want to delete ?')){
		$.ajax({
			url: 'routes.php?route=delete',
			type: 'POST',
			data: {'id': $id},
			success:function(data){
				if(data == 'success'){
					$('#tr_'+$id).remove();
				}else{
					alert('Error');
				}
			}
		});
	}
}