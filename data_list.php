<!DOCTYPE html>
<html>
	<?php require('header.php'); ?>
	<body style="padding-top: 10%;">

		<div align="center">
			<div id="list_id">
				<h2>List of data</h2>
				<div style="padding-left: 12%"><a href="index.php?page=create">Create New</a><br></div><br>
				<table border="1">
					<thead>
						<th>Name</th>
						<th>Description</th>
						<th>Course</th>
						<th>Action</th>
					</thead>
					<?php
						$data_list = dataList();
						foreach ($data_list as $list){
							echo '<tr id="tr_'.$list['id'].'">
									<td>'.$list['name'].'</td>
									<td>'.$list['description'].'</td>
									<td>'.$list['course'].'</td>
									<td><button onclick="update_list('.$list['id'].')">Update</button> || <button onclick="delete_list('.$list['id'].')">Delete</button></td>
								  </tr>';
						}
					?>
				</table>
			</div>

			<br><br><br>
			<div id="update_div" hidden>
				<fieldset>
					<legend>Update</legend>
					<table border="1" id="update_tbl">
					</table>
				</fieldset>
			</div>
		</div>

	</body>
</html>