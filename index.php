<?php

	require('db.php');

  	$page = 'main';

  	if(isset($_GET['page']))
    	$page = $_GET['page'];

  	switch ($page){
	    case 'main':
	      		require('data_list.php');
	    	break;

	    case 'create':
	    		require('create_data.php');
	    	break;

	    case 'update':
	    		require('update_data.php');
	    	break;
  	}


?>