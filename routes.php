<?php

	require('db.php');

	$route = $_GET['route'];

	switch ($route) {
		case 'create':			
				echo createData($_POST['name'], $_POST['desc'], $_POST['course']);
			break;

		case 'getdata':
				$get_user = getUser($_POST['id']);
				echo 
					'<tr>
						<th>Name</th>
						<td><input type="number" id="edt_id" value="'.$get_user['id'].'" hidden><input type="text" id="edt_name" value="'.$get_user['name'].'"></td>
					</tr>
					<tr>
						<th>Description</th>
						<td><input type="text" id="edt_desc" value="'.$get_user['description'].'"></td>
					</tr>
					<tr>
						<th>Course</th>
						<td>
							<select id="edt_course">
								<option value = "IT"'.($get_user['course'] == "IT" ? 'selected' : '').'>IT</option>
								<option value = "CRIM"'.($get_user['course'] == "CRIM" ? 'selected' : '').'>CRIM</option>
								<option value = "EDUC"'.($get_user['course'] == "EDUC" ? 'selected' : '').'>EDUC</option>
								<option value = "ENG"'.($get_user['course'] == "ENG" ? 'selected' : '').'>ENG</option>
								<option value = "HRM"'.($get_user['course'] == "HRM" ? 'selected' : '').'>HRM</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><button onclick="edit_user()">Submit</button> | <a onclick="cancel()" href="index.php?page=main">Cancel</a></td>
					</tr>';
			break;

		case 'update':

				echo updateData($_POST['edt_name'], $_POST['edt_desc'], $_POST['edt_course'], $_POST['edt_id']);
				
			break;

		case 'delete':
				echo deleteData($_POST['id']);
			break;

		default:
			# code...
			break;
	}

?>